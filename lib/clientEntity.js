 exports.constructor = function(socket){
 	var objClient = new Object();
 	objClient.socket = socket;
 	objClient.away = false;
 	objClient.nick = "";
 	objClient.channel = null;
 	objClient.awayMessage = "";
 	objClient.channels = [];
 	objClient.isOp = false;
 	objClient.modes = [];
	objClient.user = null;
	objClient.idle = null;
	objClient.realname = null;
	objClient.visible = true;
 	objClient.pswdtmp=null;
 	objClient.pswd=null;
	objClient.quitMessage = null;

 	return objClient;
}
