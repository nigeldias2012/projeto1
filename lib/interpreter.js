
﻿net = require('net');
server = require('../server/server.js');

var sockets = [];
var allClients = [];

exports.resolve = function(clients, connections, objClient, str, channelList, offlineClients)
{
	sockets = connections;
	allClients = clients;

	var comando = str.split(" ");

  switch (comando[0])
  {
    case "/quit":
      objClient.socket.end();
      if(comando[1]) objClient.quitMessage = str.slice(5);
      break;

    case "/mode":
      str = 'm' + str.slice(5);
      userModeMessage(objClient, str);
      break;

    case "/statuscanais":
      objClient.socket.write("channel list: " + channelList.length + "\n");
      statuscanais(objClient, channelList);
      break;

    case "/join":
      str = str.slice(6);
      nome_do_canal = str.split(" ");
      join(objClient, nome_do_canal, channelList, clients);
      break;

    case "/leave":
      str = str.slice(7);

        if (str.length <= 0)
        {
            // Se for passado sem nome do canal
            leave(objClient, objClient.channel, channelList, clients);
        }
        else
        {
            // Se passar o nome do canal
            if (userEstaNoCanal(objClient, str))
            {
                var canal;
                for (i = 0; i < channelList.length; i++)
                 {
                    if (channelList[i].nick == str)
                    {
                        canal = channelList[i];
                    }
                }
                leave(objClient, canal, channelList, clients);
                return;
            }
            objClient.socket.write("O nome do canal é invalido.\n");
        }
      break;

    case "/away":
      isAway(objClient, comando, true);
      break;

    case "/back":
      if(objClient.away) isAway(objClient, str.slice(5), false);
      break;

    case "/nick":
      //alterações para cadastrar uma senha com nick
      if (objClient.pswdtmp == null)
      {
        resolveNick(comando[1], objClient);
      }
      else
      {
        pass(objClient, clients, str, socket);
        objClient.pswdtmp = null;
      }
      break;

    case "/ison":
      ison(comando, objClient);
      break;

    case "/users":
      if(comando[1] == 'localhost') users(objClient, comando[1]);  
      else objClient.socket.write("[SYSTEM]: Server not found.");
      break;

    case "/wallops":
      wallops(objClient, str.slice(8));
      break;

    case "/userhost":
      userhost(objClient, str.slice(9));
      break;

    case "/die":
      server.die();
      break;

    case "/restart":
      server.restart();
      break;

    case "/pass":
      pass(objClient, clients, str);
      break;

    case "/clients":
      //criado apenas para testes ->mostra a pessoas logadas e/ou cadastradas
      for (var i = 0; i < allClients.length; i++)
      {
        objClient.socket.write("usuario :" + allClients[i].nick + "\n");
        sleep(10);
      }
      break;

    case "/who":
      who(str, comando, objClient.socket, clients);
      break;

    case "/whois":
      whois(str, comando, objClient.socket, clients);
      break;

    case "/whowas":
      whowas(str, comando, objClient.socket, offlineClients);
      break;

    case "/privmsg":
      privmsg(str, objClient.socket, objClient);
      break;

    case "/notice":
     notice(str, objClient.socket, objClient);
      break;

    default:
      objClient.socket.write("[SYSTEM]: Command not recognized.\n");
      
  }
}


exports.channelResolve = function(clients, connections, objClient, str, channelList)
{

    var nome_do_canal = str.split(" ");
    // Coloca o nome do canal em nome_do_canal[0]
    var user_esta_no_canal = userEstaNoCanal(objClient, nome_do_canal[0]);
    // Verifica se o user está no canal


    if (user_esta_no_canal == false)
    {

        objClient.socket.write("Você precisa digitar o nome de um canal que você está conectado\n");
        return;

    }
    else
    {
        for (i = 0; i < channelList.length; i++)
        {

            if (channelList[i].nick == nome_do_canal[0])
            {
                broadcast(objClient.nick + "> " + str.slice(nome_do_canal[0].length), objClient, clients, channelList[i]);
                return;
            }
        }

        objClient.socket.write("Nome do canal inexistente ou o cliente não está conectado no canal\n");
    }
}


function statuscanais(objClient, channelList) {

    // Função para ajudar no desenvolvimento do programa. Imprime a lista de canais
    // do servidor, do usuário e o canal atual do usuário.
    for (i = 0; i < channelList.length; i++) {

        // Imprime a lista de canais do servidor
  sleep(10);
        objClient.socket.write("Lista de canais: " + channelList[i].nick + "\n");

    }

    for (i = 0; i < objClient.channels.length; i++) {

        // Imprime a lista de canais do usuário.
  sleep(10);
        objClient.socket.write("Lista de canais que o usuário está conectado: " + objClient.channels[i].nick + "\n");

    }

    // Imprime o canal atual do usuário
    sleep(10);
    objClient.socket.write("Canal atual do usuário: " + objClient.channel.nick + "\n");
}



function userModeMessage(objClient, mensagem) {
    // Pega os dados do usuário e a mensagem, um comando de user mode só pode ser
    // usado se o nome do usuário for igual ao nome pedido na mensagem

    var mensagem = mensagem;
    // coloca o nome e o comando dado em um vetor

    if (mensagem.length == 1) {
        //Para ver o mode do canal, o comando vai ser recebido sem nenhum parâmetro
        // ex: /mode

        objClient.socket.write("Channel mode is: " + objClient.channel.modes + "\n");
        return;

    } else {
        var nome_e_comando = mensagem.split(" ");
        if (nome_e_comando[1] != objClient.nick) {
            // Se o cliente desejado não for o próprio cliente que digitou o comando
            // retornamos uma mensagem de erro, o comando só pode ser usado se o nome
            // enviado no parâmetro for igual ao do cliente que enviou o comando

            objClient.socket.write("Você não pode ver/alterar o mode de outro usuário.\n");
            return;
        }
        if (nome_e_comando.length == 2) {
            // Pega o nome de um cliente e informa o seu status para o cliente que
            // digitou o comando

            objClient.socket.write(">Usermode for " + objClient.nick + " is now +" + objClient.modes + "\n");
            return;

        }

        var modes = nome_e_comando[2].split("");
        // Essa variável vai pegar somente o argumento do mode, para que possamos
        // avaliar cada mode digitado
        if (/^a-zA-Z/.test(String(modes).substr(1)) == true) {
            // Verifica se os modes são somente uma string de caracteres alfabéticos
            objClient.socket.write("O mode " + modes + " é invalido. Um mode válido é uma string que" +
                " consiste de '+' ou '-' seguido de um ou mais caracteres alfabéticos\n");
            return;

        }

        if (modes[0] == "+") {
            // Adicionando um mode
            var check = [];
            var erros_adicionar = [];
            var quant_erros_adicionar = 0;
            var modes_adicionados = [];
            var quant_modes_adicionados = 0;
            for (i = 1; i < modes.length; i++) {

                // Mantendo um vetor chamado all_modes que vai ter todos os modes como
                // objetos, a funcao check_mode_adicionar verifica se é possível adicionar
                // o mode.
                check = check_mode_adicionar_user(objClient, modes[i]);
                if (check == '0') {
                    // Se não tiver problema para adicionar o mode
                    objClient.modes.push(modes[i]);
                    objClient.modes.sort();
                    modes_adicionados[quant_modes_adicionados] = modes[i];
                    quant_modes_adicionados++;
                } else if (check == '1') {
                    // Alguns modes não retornam nada


                } else {
                    // Todos os erros encontrados ao tentar adicionar os modes serão colocados
                    // em um vetor. Caso não seja adicionado nenhum mode, todos os erros serão
                    // informados, caso seja adicionado algum mode, nenhum erro vai ser
                    //informado.
                    if (erros_adicionar.indexOf(check) < 0) {

                        erros_adicionar[quant_erros_adicionar] = check;
                        quant_erros_adicionar++;

                    }
                }
            }

            if (quant_erros_adicionar == (modes.length - 1)) {
                // Mostrando os erros, se não adicionou nenhum modes

                for (i = 0; i < quant_erros_adicionar; i++) {

                    objClient.socket.write(erros_adicionar[i] + "\n");

                }
            } else {
                // Mostra os modes adicionados e os erros se tiver algum

                for (i = 0; i < quant_erros_adicionar; i++) {

                    if (erros_adicionar[i] != "Unkown MODE flag\n") {
                        objClient.socket.write(erros_adicionar[i] + "\n")
                    }
                }
                modes_adicionados.sort();
                objClient.socket.write(">User mode for " + objClient.nick + " is now +" + String(modes_adicionados) + "\n");


            }
        } else if (modes[0] == "-") {
            // Removendo um mode
            var check = [];
            var modes_removidos = [];
            var quant_modes_removidos = 0;

            for (i = 1; i < modes.length; i++) {

                check = check_mode_remover_user(objClient, modes[i]);
                if (check == "0") {
                    // Se não tiver problema para remover o mode
                    objClient.modes.splice(objClient.modes.indexOf(modes[i]), 1);
                    objClient.modes.sort();
                    modes_removidos[quant_modes_removidos] = modes[i];
                    quant_modes_removidos++;
                }
            }

            // Mostra os modes removidos
            modes_removidos.sort();
            objClient.socket.write(">User mode for " + objClient.nick + " is now -" + modes_removidos + "\n");
        } else {
            client.write("O mode " + modes + " é invalido. Um mode válido é uma string que" +
                " consiste de '+' ou '-' seguido de um ou mais caracteres alfabéticos\n");
            return;
        }
    }
}

function check_mode_adicionar_user(objClient, mode) {
    // Função que faz um check para ver se é possível adicionar um certo mod a um
    // usuário
    if (objClient.modes.indexOf(mode) > -1) {
        return '1';

    }

    if (objClient.channel.all_modes.indexOf(String(mode).toLowerCase()) > -1) {
        // Verifica se o mode é um mode que existe
        switch (mode) {
            // Verifica se o usuário pode receber o mode (alguns modes possuem
            // restrições)
            case 'a':
                if ((objClient.modes.indexOf("o") || objClient.modes.indexOf("O")) < 0 &&
                    objClient.modes.indexOf("a") < 0) {
                    return '*** You need oper and admin flag for +a';
                }
                break;

            case 'i':
                return '0';
                break;

            case 'w':
                return '0';
                break;

            case 'r':
                return '0';
                break;

            case 'o':
                return '1';
                break;

            case 'O':
                return '1';
                break;

            case 's':
                return '0';
                break;

            default:
                break;

        }
    } else {
        // Caso o mode não exista
        return "Unkown MODE flag\n";
    }
}

function check_mode_remover_user(objClient, mode) {

    if (mode == 'r') {
        return '1;';
    }

    if (objClient.modes.indexOf(mode) > -1) {
        return '0';

    } else {

        return '1';

    }
}


function resolveNick(str, objClient)
{
    if (!str) objClient.socket.write("[SYSTEM]: Too feel arguments. Correct usage: command <nick>.\n");

    else if (!checkNick(str, allClients))
    {
        if (objClient.nick !== null)
            objClient.socket.write("Nick already in use. Please, select another one.\n");
        else
        {
            objClient.socket.write("Nick already in use. Please, select another one.\n");
            objClient.socket.end();
        }
    }
    else if (objClient.nick !== null)
    {
        var tmp = objClient.nick;
        objClient.nick = str;
        objClient.socket.write("YOU are now known as " + objClient.nick + "\n");
        sleep(10);
        broadcast(tmp + " changed nickname to " + objClient.nick + "\n", objClient);
    }
    else
    {
        objClient.nick = str;
        //Print welcome message and announce cliente
        objClient.socket.write("Welcome " + objClient.nick + "!\n");
        broadcast(objClient.nick + " joined the chat.\n", objClient.socket);
    }
}


function checkNick(str, clientArray)
{
    var bool = true;
    if (clientArray[0].nick == null)
        return bool;
    for (var i = 0; i < clientArray.length; i++)
    {
        //console.log("GOT HERE! " + array[i].nick);
        if (clientArray[i].nick == str)
        {
            //console.log('SUCESS!');
            bool = false;
        }
    }
    return bool;
}

function isAway(objClient, msg, bool)
{
  if(bool)
  {
    if(objClient.away)
    {
      objClient.awayMessage = "";
      if(!msg[1]) objClient.socket.write("[SYSTEM]: No automatic reply set for away state.\n");
      for (var i = 1; i < msg.length; i++) 
        objClient.awayMessage+=msg[i]+" ";
    }
    else
    {
      objClient.socket.write("[SYSTEM]: You are marked as away.\n");            
      for (var i = 1; i < msg.length; i++) {
        objClient.awayMessage+=msg[i]+" ";
      }
      if(!msg[1]){ objClient.socket.write("[SYSTEM]: No automatic reply set for away state.\n"); objClient.awayMessage="";}
      broadcast( "[INFO]: " + objClient.nick + " is away.\n", objClient.socket);
      objClient.away = true;
    }
  }
  else
  {
    objClient.awayMessage="";
    objClient.socket.write("[SYSTEM]: You are no longer marked as away.\n");
    broadcast("[INFO]: " + objClient.nick + " is not away anymore\n", objClient.socket);
    objClient.away = false;
  }
}

function ison(arrayNicks, objClient)
{
  var tmp = 0;
  for (var i = 0; i < arrayNicks.length; i++) {
    tmp+=arrayNicks[i].length;
  }
  //console.log("ITS ON!");
  var time = 10;
  if(!arrayNicks[1]) objClient.socket.write("[SYSTEM]: Too few arguments. USAGE: ISON <nick1, nick2, nick3...>\n");
  
  else if(tmp < 512) //deve ser menor que 512 caracteres
  { 
    var bool = false;
    for (var i = 1; i < arrayNicks.length; i++)
    {
      for (var j = 0; j < allClients.length; j++)
      {
        if(allClients[j].nick == arrayNicks[i])
        {
          sleep(time);
          objClient.socket.write("User " + arrayNicks[i] +" is on.\n");
          bool = true;
        }
      }
      if(!bool) 
      { 
                                                                sleep(time);
        objClient.socket.write("Nick '"+arrayNicks[i]+"' not found.\n");
      }
      bool = false;
    }
  }
  else objClient.socket.write("[SYSTEM]: Too many nicknames, try again!\n");
}


function sleep(time)
{
    var stop = new Date().getTime();
    while(new Date().getTime() < stop + time){;}
}


//Fornece informações de um cliente offline
function whowas(tmp, cmd, client, offlineClients)
{
  // variável para definir o conteúdo da máscara em alguns casos
  var mask = null;
  // flag que determina se algum nick especificado pela máscara foi encontrado ou não
  var maskmatch = false;

  if (tmp.length < 9)
  {// nenhum argumento depois de /whois foi fornecido
    client.write("[WHOWAS]: No nickname given.\n");
    // indicar o fim do retorno de informações do WHOIS
    client.write("End of WHOWAS\n");
    return;
  }

  // checar se o comando recebeu a máscara * (que indica todos os clientes)
  if (cmd[1] == "*")
  {// fornecer informação sobre todos os clientes
    for (var i=0; i < offlineClients.length; i++)
    {
        whowasReply(client, offlineClients, i);
    }
    // indicar o fim do retorno de informações do WHOIS
    client.write("End of WHOWAS\n");
    return;
  }

  // checar se o comando recebeu a máscara ? (que indica nicks de apenas um caractere)
  if (cmd[1] == "?")
  {// fornecer informação sobre todos os clientes com nick de 1 caractere
    for (var i=0; i < offlineClients.length; i++)
    {
      if (offlineClients[i].nick != null)
      {
        if (offlineClients[i].nick.length == 1)
            whowasReply(client, offlineClients, i);
      }
    }
    // indicar o fim do retorno de informações do WHOIS
    client.write("End of WHOWAS\n");
    return;
  }

  // verificar se uma mascara foi especificada que começa com *
  if (cmd[1].startsWith('*'))
  {
    // definir a máscara como tudo que vem depois do *
    mask = cmd[1].substring(1);
    // fornecer informações de todos clientes que terminam com a máscara fornecida
    for (var i = 0; i < offlineClients.length; i++)
    {
      if (offlineClients[i].nick.endsWith(mask))
      {
        whowasReply(client, offlineClients, i);
        maskmatch = true;
      }
    }
    if (!maskmatch)
      client.write("[WHOWAS]: No such nickname.\n");
    // indicar o fim do retorno de informações do WHOIS
    client.write("End of WHOWAS\n");
    return;
  }

  // verificar se uma mascara foi especificada que termina com *
  if (cmd[1].endsWith('*'))
  {
    // definir a máscara como tudo que vem antes do *
    mask = cmd[1].substring(0, cmd[1].length-1);
    // fornecer informações de todos clientes que terminam com a máscara fornecida
    for (var i = 0; i < offlineClients.length; i++)
    {
      if (offlineClients[i].nick.startsWith(mask))
      {
        whowasReply(client, offlineClients, i);
        maskmatch = true;
      }
    }
    if (!maskmatch)
      client.write("[WHOWAS]: No such nickname.\n");
    // indicar o fim do retorno de informações do WHOIS
    client.write("End of WHOWAS\n");
    return;
  }

  // verifica se o nick fornecido existe
  for (var i=0; i < offlineClients.length; i++)
  {// procura o nick fornecido entre os clientes
    if (offlineClients[i].nick == cmd[1])
    {// caso tenha encontrado e estiver visível
      whowasReply(client, offlineClients, i);
      // indicar o fim do retorno de informações do WHOIS
      client.write("End of WHOWAS information about " + offlineClients[i].nick + "\n");
      return;
    }
  }

  // se chegou aqui, o nick não foi encontrado
  client.write("[WHOWAS]: No such nickname.\n");
  // indicar o fim do retorno de informações do WHOIS
  client.write("End of WHOWAS\n");
  return;
}

function whowasReply(client, offlineClients, i)
{

  // fornecer informações de user (caso exista), remote adress e real name
  client.write("<" + offlineClients[i].nick + "> ");
  if (offlineClients[i].user != null)
    client.write("<" + offlineClients[i].user + "> ");
  client.write("<" + client.remoteAddress + "> ");
  if (offlineClients[i].realname != null)
    client.write(offlineClients[i].realname + "\n");
  else
    client.write("No real name\n");
}




// Fornece informações de um cliente
function whois(tmp, cmd, client, clients)
 {
  // variável para definir o conteúdo da máscara em alguns casos
  var mask = null;
  // flag que determina se algum nick especificado pela máscara foi encontrado ou não
  var maskmatch = false;


 	if (tmp.length < 8)
 	{// nenhum argumento depois de /whois foi fornecido
 		client.write("[WHOIS]: No nickname given.\n");
    // indicar o fim do retorno de informações do WHOIS
    client.write("End of WHOIS\n");
 		return;
 	}

  // checar se o comando recebeu a máscara * (que indica todos os clientes)
  if (cmd[1] == "*")
  {// fornecer informação sobre todos os clientes
    for (var i=0; i < clients.length; i++)
    {
      if (clients[i].visible)
        whoisReply(client, clients, i);
    }
    // indicar o fim do retorno de informações do WHOIS
    client.write("End of WHOIS\n");
    return;
  }

  // checar se o comando recebeu a máscara ? (que indica nicks de apenas um caractere)
  if (cmd[1] == "?")
  {// fornecer informação sobre todos os clientes com nick de 1 caractere
    for (var i=0; i < clients.length; i++)
    {
    	if (clients[i].nick != null)
    	{
    		if (clients[i].nick.length == 1 && clients[i].visible)
        		whoisReply(client, clients, i);
    	}
    }
    // indicar o fim do retorno de informações do WHOIS
    client.write("End of WHOIS\n");
    return;
  }

  // verificar se uma mascara foi especificada que começa com *
  if (cmd[1].startsWith('*'))
  {
    // definir a máscara como tudo que vem depois do *
    mask = cmd[1].substring(1);
    // fornecer informações de todos clientes que terminam com a máscara fornecida
    for (var i = 0; i < clients.length; i++)
    {
      if (clients[i].nick.endsWith(mask) && clients[i].visible)
      {
        whoisReply(client, clients, i);
        maskmatch = true;
      }
    }
    if (!maskmatch)
      client.write("[WHOIS]: No such nickname.\n");
    // indicar o fim do retorno de informações do WHOIS
    client.write("End of WHOIS\n");
    return;
  }

  // verificar se uma mascara foi especificada que termina com *
  if (cmd[1].endsWith('*'))
  {
    // definir a máscara como tudo que vem antes do *
    mask = cmd[1].substring(0, cmd[1].length-1);
    // fornecer informações de todos clientes que terminam com a máscara fornecida
    for (var i = 0; i < clients.length; i++)
    {
      if (clients[i].nick.startsWith(mask) && clients[i].visible)
      {
        whoisReply(client, clients, i);
        maskmatch = true;
      }
    }
    if (!maskmatch)
      client.write("[WHOIS]: No such nickname.\n");
    // indicar o fim do retorno de informações do WHOIS
    client.write("End of WHOIS\n");
    return;
  }

  // verifica se o nick fornecido existe
 	for (var i=0; i < clients.length; i++)
 	{// procura o nick fornecido entre os clientes
 		if (clients[i].nick == cmd[1] && clients[i].visible)
 		{// caso tenha encontrado e estiver visível
      whoisReply(client, clients, i);
      // indicar o fim do retorno de informações do WHOIS
      client.write("End of WHOIS information about " + clients[i].nick + "\n");
 			return;
 		}
 	}

 	// se chegou aqui, o nick não foi encontrado
 	client.write("[WHOIS]: No such nickname.\n");
  // indicar o fim do retorno de informações do WHOIS
  client.write("End of WHOIS\n");
 	return;
}





function whoisReply(client, clients, i)
{
  // fornecer informações de user (caso exista), remote adress e real name
  client.write("<" + clients[i].nick + "> ");
  if (clients[i].user != null)
    client.write("<" + clients[i].user + "> ");
  client.write("<" + client.remoteAddress + "> ");
  if (clients[i].realname != null)
    client.write(clients[i].realname + "\n");
  else
    client.write("No real name\n");

  // fornecer informações de quais canais ele está conectado
  client.write("<" + clients[i].nick + "> : ");
  if (clients[i].channels == null)
    client.write("Connected to no channel.\n");
  else
  {
    client.write("member of ");
    for (var j=0; j < clients[i].channels.length; j++)
      client.write("<" + clients[i].channels[j] + ">");
    client.write("\n");
  }

  // fornecer informação do tempo que o cliente está idle
  client.write("<" + clients[i].nick + "> : ");
  if (clients[i].idle == null)
    client.write("No idle time information.\n");
  else
    client.write("idle for " + clients[i].idle + " seconds.\n");

  // avisar se o cliente está away
  if (clients[i].away)
    client.write("<" + clients[i].nick + "> : is away.\n");

  // avisar se o cliente é um operador
  if (clients[i].isOp)
    client.write("<" + clients[i].nick + "> : is an operator.\n");
}


function notice(cmd, client, objClient){
	var target = ""; //o cliente que ira receber a mensagem
	var msg = "";    //a mensagem a ser enviada

 	// Identifica o target e msg depois do comando "/notice "
	for(var i = 8; i< cmd.length; i++)
       	{
          	if(cmd[i] == " "){
	             	msg = cmd.slice(i+1) + "\n"; //guarda a mensagem tirando o espaço
             	break;
          	}
          	else
          	{
	          	target+=cmd[i]; //salva caracter por caracter até chegar em um espaço
        	}
       	}
	var nicks = [];

	if(target == ""){
		client.write("No recipient given (NOTICE)\n"); // 411    ERR_NORECIPIENT
		return;
	} else if (target.startsWith('#') || target.startsWith('$'))
	{
	//Tratamento de masks
	//# ---> Marcação para host maks
	//$ ----> Marcação para server mask
	//A wildcard deve estar no inicio

	//retirando # ou $
	var mask = target.slice(1).trim();
	        if (mask.startsWith('*')){
        	// definir a máscara como tudo que vem depois do *
   		 mask = mask.substring(1);

   		 	for(var i = 0; i < allClients.length; i++){
				//verifica se a mask está no fim do nick
	 			if(allClients[i].nick.endsWith(mask))
					nicks.push(allClients[i].nick);
   		 	}
        	} else if(mask.endsWith('*')){
        	// definir a máscara como tudo que vem antes do *
        	 mask = mask.substring(0, mask.length-1);
        	 	for(var i = 0; i < allClients.length; i++){
			//verifica se a mask está no começo do nick
			if(allClients[i].nick.startsWith(mask))
					nicks.push(allClients[i].nick);
			}
        	}
    	} else
    		nicks[0] = target;


    	for(var i = 0; i < nicks.length; i++){
    		var pos = getPosNick(nicks[i], allClients); //pega a posicao de acordo com o nick

		if(msg == "")
			client.write("No text to send\n"); // 412    ERR_NOTEXTTOSEND
   		else if(pos == -1)
			client.write("The nickname \""+nicks[i]+"\" does not exist.\n");
 		else
 			sendMessage(msg, pos, objClient);				  //envia a mensagem
    	}
}


function privmsg(cmd, client, objClient){

	var target = ""; //o cliente que ira receber a mensagem
	var msg = "";    //a mensagem a ser enviada

 	// Identifica o target e msg depois do comando "/privmsg "
	for(var i = 9; i< cmd.length; i++)
       		{
          		if(cmd[i] == " "){
	             		msg = cmd.slice(i+1) + "\n"; //guarda a mensagem tirando o espaço
             		break;
          		}
          		else
          		{
	             		target+=cmd[i]; //salva caracter por caracter até chegar em um espaço
          		}
       		}
	var nicks = [];

	if(target == ""){
		client.write("No recipient given (PRIVMSG)\n"); // 411    ERR_NORECIPIENT
		return;
	} else if (target.startsWith('#') || target.startsWith('$'))
	{
	//Tratamento de masks
	//# ---> Marcação para host maks
	//$ ----> Marcação para server mask
	//A wildcard deve estar no inicio

	//retirando # ou $
	var mask = target.slice(1).trim();
	        if (mask.startsWith('*')){
        	// definir a máscara como tudo que vem depois do *
   		 mask = mask.substring(1);

   		 	for(var i = 0; i < allClients.length; i++){
				//verifica se a mask está no fim do nick
	 			if(allClients[i].nick.endsWith(mask))
					nicks.push(allClients[i].nick);
   		 	}
        	} else if(mask.endsWith('*')){
        	// definir a máscara como tudo que vem antes do *
        	 mask = mask.substring(0, mask.length-1);
        	 	for(var i = 0; i < allClients.length; i++){
			//verifica se a mask está no começo do nick
			if(allClients[i].nick.startsWith(mask))
					nicks.push(allClients[i].nick);
			}
        	}
    	} else
    		nicks[0] = target;


    	for(var i = 0; i < nicks.length; i++){
    		var pos = getPosNick(nicks[i], allClients); //pega a posicao de acordo com o nick

		if(msg == "")
			client.write("No text to send\n"); // 412    ERR_NOTEXTTOSEND
   		else if(pos == -1)
			client.write("The nickname \""+nicks[i]+"\" does not exist.\n");
 		else{
 			sendMessage(msg, pos, objClient);				  //envia a mensagem
 				if(allClients[pos].away && allClients[pos].awayMessage != "")	//se o destinatario estiver away
 					client.write(nicks[i]+ ": " +allClients[pos].awayMessage+"\n");	//resposta automática
 		}
    	}
}

//retorna a posicao do nick no vetor
function getPosNick(str, array){
 var pos = -1;
//percorre o array até encontrar o nick
 for (var i = 0; i  < array.length; i++) {
 	 if (array[i].nick == str) {
 		  pos = i;
 		  break;
  	 }
 }
 return pos;
}


  //Retorna informações do server correspondente ao parâmetro da mask
  //Obs.: cmd --> vetor com strings subdivididas do comando passado
	function who(tmp, cmd, client)
	{
	  var mask = null;
	  var maskmatch = false;

	  if(tmp.length < 5 || cmd[1] == "0" || cmd[1] == "*")
	  { //Se comando foi chamado sem nenhum parametro na mask (/who ) ---> Todos os usuários devem ser retornados

	    client.write("List of all visible users \n"); // lembrar de formatar usuários visiveis
	    for (var i=0; i < client.length; i++){
	      if(client[i].visible){
	        whoReply(client, i);
	        //client.write(i+" ) < Nick: " + client[i].nick + "> ");
	      }
	    }
	    client.write("End of WHO\n");
	    return;
	  } else {
	    //Casos em que wildcard estão no inicio
	    if (cmd[1].startsWith("*"))
	    {
	        mask = cmd[1].slice(1);

	      if(cmd[1].startsWith("*")){
	        for (var i=0; i < client.length; i++)
	        {
	          if(cmd[2] == "o"){ //Se user é um operador
	            if (client[i].visible && client[i].nick.endsWith(mask) && client[i].operator){
	              whoReply(client, i, true);
	              maskmatch = true;
	            }
	          } else if (client[i].visible && client[i].nick.endsWith(mask)){
	              whoReply(client, i, false);
	              maskmatch = true;
	          }
	        }
	        if (!maskmatch)
	          client.write("[WHO]: Mask not described.\n");
	        }
	      } else if(cmd[1].startsWith("#")){		//Comando para verificação de usuário conectados adeterminado canal

					mask = cmd[1].slice(1);

					for(var i=0; i < client.length; i++){
						if (client[i].visible) {
							for(var j=0; j<client[i].channels.length; j++){
								if(client[i].channels[j] == mask){
									whoReply(client, i, false);
		              maskmatch = true;
								}
							}
	          }
					}
				} else { //caso mais simples, em que enhuma mask é passada
						mask = cmd[1];
						for (var i=0; i < client.length; i++)
		        {
		          if(cmd[2] == "o"){ //Se user é um operador
		            if (client[i].visible && client[i].nick.endsWith(mask) && client[i].operator){
		              whoReply(client, i, true);
		              maskmatch = true;
		            }
		          } else if (client[i].visible && client[i].nick.endsWith(mask)){
		              whoReply(client, i, false);
		              maskmatch = true;
		          }
		        }
		        if (!maskmatch)
		          client.write("[WHO]: Mask not described.\n");
				}
	      // indicar o fim do retorno de informações do WHOIS
	      client.write("\nEnd of WHO\n");
	      return;
	    }

	    if (cmd[1].endsWith('*')){
	        mask = cmd[1].substring(0, cmd[1].length-1);

	        if(cmd[1].endsWith('*')){
	          for (var i=0; i < client.length; i++)
	          {
	            if(cmd[2] == 'o'){ //Se user é um operador
	              if (client[i].visible && client[i].nick.startsWith(mask) && client[i].operator){
	                whoReply(client, i, true);
	                maskmatch = true;
	              }
	            } else if (client[i].visible && client[i].nick.startsWith(mask)){
	              whoReply(client, i, false);
	              maskmatch = true;
	            }
	          }
	          if (!maskmatch)
	            client.write("[WHO]: Mask not described.\n");
	        }
	        client.write("End of WHO\n");
	        return;
	    }

	  }


	function whoReply(client, i, operador)
	{
	  var usuario = "";
	  var realName = "";
	  var canal = "";
	  var ip = "";
	  var operador_usr = "";

	  //verificar informações disponíveis
	  if (client[i].user != null)
	    usuario = client[i].user;

	  if (client[i].realname != null)
	    realName = client[i].realname;

	  if (client[i].channel == null)
	    canal = "no channel.";
	  else
	    canal = " #" + client[i].channel+ " ";
	  // fornecer informações de user (caso exista), remote adress e real name
	    ip =  client.remoteAddress;
	  // avisar se o cliente é um operador
	  if(operador){
	    if (client[i].operator)
	      operador_usr = client[i].nick +" is an operator!.\n";
	    } else {
	      operador_usr = " ";
	    }

	    client.write("User "+client[i].nick +" ("+usuario+"@"+ip+") '"+client[i].nick+"' member of "+canal+". \n");
	  }


//Somente um servidor,
//assim sendo: /users localhost
function users(objClient, server)
{
    var time = 10;
    var nameServer = server.toString();     
    objClient.socket.write("[SYSTEM]: Users at server "+nameServer+":\n");
    for(i = 0; i < allClients.length; i++)
    {
        var stop = new Date().getTime();
    while(new Date().getTime() < stop + time){;}
        objClient.socket.write(allClients[i].nick +"\n" );
    }
}

function wallops(objClient, cmd)
{ 
  var cont = 0; 
  for(var i=0; i < allClients.length; i++)
  {
    if (allClients[i].modes == 'w'){
      allClients[i].socket.write("(" + objClient.nick + ") to wallops: " + cmd + "\n");
      cont++;
    }
  }
  if(cont == 0){
    sleep(10);
    objClient.socket.write("[SYSTEM]: None user connected receives wallops\n");
  }else objClient.socket.write("[SYSTEM]: Text sent to " + cont + " users.\n");
}

function userhost(objClient, cmd)
{
    var array = cmd.slice(1).split(" "); //transforma o cmd em um array;
    if (!array[0]) objClient.socket.write("[SYSTEM]: Too few arguments. Command requires nickname(s)."); //verifica se o comando foi sem nicks
    else if (array.length > 5) objClient.socket.write("[SYSTEM]: Command exceeded five nicks limit."); //verifica se o comando ultrapassou 5 nicks
    else
    {
        for (var i = 0; i < array.length; i++)
        {
            for (j = 0; j < allClients.length; j++)
            {
                if (array[i] == allClients[j].nick)
                {
                    objClient.socket.write(allClients[j].nick + " RemoteAddress: " + allClients[j].socket.remoteAddress + "\n");
                    j = allClients.length + 1;
                }
            }
            if (j == allClients.length) objClient.socket.write("Nick " + array[i] + " doesn't exist.\n");
        }
    }
}


function broadcast(message, sender, clients, canal)
{
    var channel;
    if(clients == null)
    {
        sockets.forEach(function(client)
        {
            if(client === sender) return; //Nao e preciso enviar a msg para o sender/remetente
            client.write(message);

        });
        //Escreve na tela do server também
        process.stdout.write(message);

    }
    else
    {
        if(canal == null) channel = sender.channel;
        
        else channel = canal;

        clients.forEach(function(client)
        {

            if (client === sender) return; //Nao e preciso enviar a msg para o sender/remetente

            for (i = 0; i < client.channels.length; i++)
            {
                if(client.channels[i].nick == channel.nick)
                    client.socket.write(channel.nick + " | " + message);
            }
        });

        //Escreve na tela do server também
        process.stdout.write(channel.nick + " | " + message);
    }
}

function pass(objClient, clients, str)
{
    if (str.split(" ")[0] == '/pass')
    {
        if (str.split(" ").length == 2 && str.split(" ")[1].length >= 6 && str.split(" ")[1].length <= 10)
        {
            objClient.pswdtmp = str.split(" ")[1].trim();
            socket.write("entre com a combinação nick/user\n");
        }
        else
        {
            objClient.socket.write("senha fora do padrão!\n");
        }

    }
    else if (str.split(" ")[0] == '/nick')
    {
        if (checkNick(str.slice(5).trim(), clients))
        {
            var objCl = clientEntity.constructor(null);
            objCl.nick = str.slice(5).trim();
            allClients.push(objCl);
            objClient.pswdtmp = null;
            objClient.socket.write("usuário e senha cadastrado com sucesso\n");
        }
        else
        {
            objClient.socket.write("usuario já está em uso ou já é cadastrado. Repita a operação\n");
        }
    }
    else objClient.socket.write("command don't recognized!\n");
}


function userEstaNoCanal(objClient, channelName, channel) {

    if (channelName == null) {
        channelName = channel.nick;

    }

    for (i = 0; i < objClient.channels.length; i++) {

        if (objClient.channels[i].nick == channelName) {

            return true;

        }

    }
    return false;
}

function join(objClient, nome_do_canal, channelList, clients) {

    for (i = 0; i < channelList.length; i++) {


        if (channelList[i].nick == nome_do_canal) {

            var canal = channelList[i];
            objClient.channel = canal;
            objClient.socket.write("Você entrou no canal " + canal.nick + "\n");

            if (!userEstaNoCanal(objClient, nome_do_canal)) {

                objClient.channels.push(canal);
                canal.clients.push(objClient);
                broadcast(objClient.nick + " joined " + nome_do_canal+"\n", objClient, clients, canal);

            }

            return;
        }
    }
    objClient.socket.write("Canal inexistente. \n");


}


function leave(objClient, channel, channelList, clients) {


    var user_esta_no_canal = userEstaNoCanal(objClient, null, channel);
    // Verifica se o usuario está no canal

    if (userEstaNoCanal) {

        broadcast("User " + objClient.nick + " has left the channel.", objClient, clients, channel);
        // Anuncia a saída do usuário e remove ele do vetor de usuarios do canal
        channel.clients.splice(channel.clients.indexOf(objClient), 1);
        objClient.channels.splice(objClient.channels.indexOf(channel), 1);
        if (objClient.channels.length < 1) {
            // Se o usuário sair de um canal e ele não estiver em nenhum outro ele vai
            // ser kitado do servidor
            objClient.socket.end();
            return;

        } else if (objClient.channel == channel) {
            // Se o usuário tiver saído do canal atual em que ele estava conectado,
            // o canal que estiver em channels[0] passará a ser o canal atualjoi
            join(objClient, objClient.channels[0].nick, channelList, clients);

        }


    } else {
        return;
    }
}
